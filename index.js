// Js Comments ctrl+/
// They are important, acts as a guide for the programmer
// Comments are disregarded no matter how long it may be.
	 //Multi-line comments ctrl+shift+/
	/*This
	is 
	a
	multi
	line
	comment*/

// Statements
// Programming instruction that we tell the computer to perform.
// Statements usually ends with semicolon (;)

// Syntax
// It is the set of rules that describes how statements must be constructed.


// alert("Hello Again!");

// this is a statement with correct syntax
console.log("Hello World!");

//js is a loose type programming language
console . log ( "   hello world! "   );

// and also with this
console.
log
(
"Hello Again"
)

// [SECTION] for variables
// it is used as a container or storage

// declaring variables - tells our device that a variable name is created and is to store data. 
// Syntax - let/const variableName ; 

//"let" is a keyword that is usually used to declare a variable.

let myVariable; 
let hello;

console.log(myVariable);
console.log(hello);

// sign stands for initialization, that means giving a value to a variable.

// guidelines for declaring a variable
/*
	1. use let keyword followed by a variable name and then followed again by the assignment =.
	2. variable names should start with a lowercase letter.
	3. for constant variables we are using const keyword.
	4. variable name should be comprehensive or descriptive.
	5. do not use spaces on the declared variables.
	6. All variable names should be unique.


*/
// Different casing styles
// camel case - thisIsCamelCasing
// snake case - this_is_snake_casing
// kebab case - this-is-kebab-casing

// this is a good variable name
let firstName = "Michael"; 

// bad variable
let pokemon = 25000;

// declaring and initializing variables
// syntax -> let/const variableName = value;

let productName = "desktop computer";
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// re-assigning a value to a variable.
productPrice = 25000;
console.log(productPrice);
console.log(productPrice);
console.log(productPrice);

let friend = "kate";
friend = "jane";
console.log(friend);

let supplier;
supplier = "john Smith tradings"
console.log(supplier);

supplier = "Zuitt Store";
console.log(supplier);

// let vs const
// let variables values can be changed, we can re-assign new values.
// const variables values cannot be changed.

const hoursInAday = 24;
console.log(hoursInAday);

const pie = 3.14;
console.log(pie);

// local/global scope variables
let outerVariable = "hi";
{
	let innerVariable = "hello Again"; //this is a local variable
	console.log(outerVariable);
	console.log(innerVariable);

}

console.log(outerVariable);
// console.log(innerVariable); this will throw error, innerVariable is not accessible since it is enclosed with curly braces.

//multiple variable declartion
// multiple variables may be declared in one line.
// convenient and its is easier to read the code.

// let productCode = "DC017";
// let productBrand = "Dell";

let productCode = "DC017", productBrand = "Dell";
console.log(productCode, productBrand);

// let is reserved keyword
/*const let = "hello";
console.log(let);     this will throw an error*/

 // [SECTION] Data Types

//  Strings - are series of characters that creates a word.

let country = "Philippines";
let province = "Metro Manila";

// Concatenating String, using + symbol
// Combining string values.

// Metro Manila, Philippines
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);

//  escape character (\)
/*https://www.tutorialspoint.com/escape-characters-in-javascript*/
// "\n" refers to creating a new line between text

let mailAddress = "Metro Manila \n\nPhillipines";
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message);
message = 'Jonn\'s employees went home early';
console.log(message);

//Numbers
//Integers or whole numbers

let headcount = 26;
console.log(headcount);

//Decimal or fractions
let grade = 98.7;
console.log(grade);

// Exponential notation
let planetDistance = 2e10;
console.log(planetDistance);

// Combining text and strings
console.log("John's grade last quarter is " + grade);

// boolean
// we have 2 boolean values, true and false

let isMarried = false;
let isGoodConduct = true;

console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);

// Arrays
// are special kind of data type it
// used to store multiple values with the same data types
// syntax -> let/const arrayName = [elementA, elementB, ....];

let grades = [98.7, 92.1, 90.2];
console.log(grades);

let details = ["John", "Smith", 32, true]
console.log(details);

// Objects
// Holds properties that descrbies the variable
// syntax -> let/const objectName = {propertyA: value, propertyB: value, ..}

let person = {
	fullName: "Juan Dela Cruz",
	age: 35,
	contact: ["0123456789","987654321"],
	address:{
		houseNumber:"345",
		city: "Manila",
	}
}
console.log(person);

let myGrades = {
	firstGrading: 98.7,
	secondGrading: 92.1,
	thirdGrading: 90.2,
	fourthGrading: 94.6,
}

console.log(myGrades);


//checking of data type
console.log(typeof grades)
console.log(typeof person)
console.log(typeof isMarried)

// in programming we always start counting from 0
// [index0, index1, index2]
const anime = ["one piece", "one punch man", "attack on titan"];
// anime = ["kimetsu no yaiba"];   this will throw error
anime[0] = "kimetsu no yaiba";
console.log(anime)

//null vs undefined
// null
let spouse = null;
//undefined
let spouse; 

